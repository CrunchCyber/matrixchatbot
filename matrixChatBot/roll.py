import random


class Roll():
    def __init__(self,seed):
        random.seed(seed)

    def return_random_number(self,query):
        numbers = self.parse_numbers(query)
        return str(random.randint(numbers[0], numbers[1]))

    @staticmethod
    def parse_numbers(query):
        if len(query) is 1:
            return [1,6]
        elif ',' in query:
            numbers = query.split(',')
            if int(numbers[0]) < int(numbers[1]):
                return [int(numbers[0]), int(numbers[1])]
            else:
                return [int(numbers[1]), int(numbers[0])]
        else:
            if int(query) > 0:
                return [0, int(query)]
            else:
                return [query, 0]
