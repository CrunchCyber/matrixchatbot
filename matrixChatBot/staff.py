"""staff"""
import matrixChatBot.ParseUsers


class Staff:

    def add(self, sender, room_id, user):
        raise NotImplementedError("Class %s doesn't implement a Method()" % self.__class__.__name__)

    def remove(self, sender, room_id, user):
        raise NotImplementedError("Class %s doesn't implement a Method()" % self.__class__.__name__)

    def is_staff(self, user, room):
        raise NotImplementedError("Class %s doesn't implement a Method()" % self.__class__.__name__)

    def read_list(self):
        raise NotImplementedError("Class %s doesn't implement a Method()" % self.__class__.__name__)


class Mod(Staff):
    def __init__(self):
        self.mods = matrixChatBot.ParseUsers.read_users('mods')

    def add(self, sender, room_id, user):
        if Admin.is_staff(sender):
            if user in self.mods:
                self.mods[user].append(room_id)
            else:
                self.mods[user] = [room_id]
            matrixChatBot.ParseUsers.write_to_file(self.mods)
            return 'User is now a mod for this room'

    def remove(self, sender, room_id, user):
        if Admin.is_staff(sender):
            if user in self.mods:
                if len(self.mods[user]) is 1:
                    del self.mods[user]
                    matrixChatBot.ParseUsers.write_to_file(self.mods)
                else:
                    self.mods[user].remove(room_id)
                return 'User is no longer a mod'
            else:
                return 'User never was a mod in this room'

    def is_staff(self, user, room):
        return room in self.mods[user]

    def read_list(self):
        self.mods = matrixChatBot.ParseUsers.read_users('mods')


class Admin(Staff):
    def __init__(self):
        self.admins = matrixChatBot.ParseUsers.read_users('admins')

    def add(self, sender, room_id, user):
        if Admin.is_staff(sender):
            if user in self.admins:
                self.admins[user].append(room_id)
            else:
                self.admins[user] = [room_id]
            matrixChatBot.ParseUsers.write_to_file(self.admins)
            return 'User is now a mod for this room'

    def remove(self, sender, room_id, user):
        if Admin.is_staff(sender):
            if user in self.admins:
                if len(self.admins[user]) is 1:
                    del self.admins[user]
                    matrixChatBot.ParseUsers.write_to_file(self.admins)
                else:
                    self.admins[user].remove(room_id)
                return 'User is no longer a admin'
            else:
                return 'User never was a mod in this admin'

    def is_staff(self, user, room):
        return room in self.admins[user]

    def read_list(self):
        self.admins = matrixChatBot.ParseUsers.read_users('admins')
