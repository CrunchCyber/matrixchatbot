# TODO fix ban message in case non mod/admin tries to ban someone
# TODO give mods/admins ban treatment
# TODO add more functionality (bitcoin liquidations)
import sys
import re
import asyncio
from importlib import util
from nio import (AsyncClient, SyncResponse, RoomMessageText)

import human_rights
import bad_words




# main function to parse the incomming messages
def parse_chat(event, room_id):
    message = event.body
    sender = event.sender

    if message.startswith('!cryptoboy'):
        if sender in ban.ban_list:
            return 'access denied'
        message = message.replace('!cryptoboy', '').strip()
        message = message.split(" ")
        if message[0] == 'roll':
            roll = Roll(sender)
            return roll.return_random_number(message[1])
        elif message[0] == 'h' or message[0] == 'help':
            return 'soon here will be a help section, \n for now im testing html'
        elif message[0] in bad_words.word_list:
            return human_rights.right
        elif message[0] == 'ban':
            return ban.ban(sender, message)
        elif message[0] == 'unban':
            return ban.unban(sender, message)
        elif message[0] == 'set' or message[0] == 'kill':
            return mods.mods(sender, message)


async def main():
    response = await async_client.login(config['password'])
    print(response)

    with open("next_batch", "r") as next_batch_token:
        async_client.next_batch = next_batch_token.read()

    while True:
        sync_response = await async_client.sync(30000)

        with open("next_batch", "w") as next_batch_token:
            next_batch_token.write(sync_response.next_batch)

        if len(sync_response.rooms.join) > 0:
            joins = sync_response.rooms.join
            for room_id in joins:
                for event in joins[room_id].timeline.events:
                    if config["botID"] not in event.sender:
                        if hasattr(event, 'body'):
                            content_text = parse_chat(event, room_id)
                            content = {"body" : content_text , "msgtype" : "m.text"}
                            await async_client.room_send(room_id, 'm.room.message', content)

if __name__ == "__main__":

    # parse parameters and save as config
    config = {}
    config["matrixServer"] = sys.argv[1]
    config["username"] = sys.argv[2]
    config['password'] = sys.argv[3]
    split_url = re.split('/', config["matrixServer"])[-1]
    config["botID"] = '@' + config["username"] + ':' + split_url

    # login to matrix
    async_client = AsyncClient(config["matrixServer"], config["username"])

    #load needed things
    ban = Ban()
    ban.read_ban_list()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
