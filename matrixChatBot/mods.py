mod_list = ['@butterbutt:matrix.thisisjoes.site']
admin_list = ['@the_flawless_gazelles:matrix.kiwifarms.net']

def mods(sender, message):
    if sender in admin_list:
        if message[0] == 'set':
            if message[1] not in mod_list and message[1] not in admin_list:
                mod_list.append(message[1])
                return 'User ' + message[1] + ' is now a moderator'
            else:
                return 'User is already a moderator'

        if message[1] == 'kill':
            if message[1] in mod_list:
                mod_list.remove(message[1])
                return 'User ' + message[1] + ' is no longer a moderator'
            else:
                return 'User is not a modetator'
