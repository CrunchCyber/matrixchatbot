"""Class to parse incoming chat messages"""
import matrixChatBot.ban
import matrixChatBot.staff
from matrixChatBot.roll import Roll


class ParseChat:
    def __init__(self, config, room_id, event):
        self.room_id = room_id
        self.sender = event.sender
        self.message = event.body
        self.config = config
        self.bans = matrixChatBot.ban.Ban()
        self.mods = matrixChatBot.staff.Mod()
        self.admins = matrixChatBot.staff.Admin()

    """parse incoming message"""
    def parse_chat(self):
        if not self.message.startswith(self.config['botName']):
            return
        if self.user_banned():
            return

        self.strip_bot_name()
        self.split_message()

        # parse help
        if self.message[0] is 'h' or self.message[0] is 'help':
            return 'help is WIP'

        # parse roll
        if self.message[0] is 'roll':
            roll = Roll(self.sender)
            return roll.return_random_number(self.message[1])

        # parse bans
        if self.message[0] is 'ban':
            ban = matrixChatBot.ban.Ban()
            return ban.ban(self.sender, self.room_id, self.message[1])
        if self.message[0] is 'unban':
            ban = matrixChatBot.ban.Ban()
            return ban.unban(self.sender, self.room_id, self.message[1])

        # parse staff actions
        if self.message[0] is 'mod' and self.message[1] is 'add':
            return self.mods.add(self.sender, self.room_id, self.message[2])
        if self.message[0] is 'mod' and self.message[1] is 'remove':
            return self.mods.remove(self.sender, self.room_id, self.message[2])
        if self.message[0] is 'admin' and self.message[1] is 'add':
            return self.admins.add(self.sender, self.room_id, self.message)
        if self.message[0] is 'admin' and self.message[1] is 'remove':
            return self.admins.remove(self.sender, self.room_id, self.message[2])

    """check if user is banned"""
    def user_banned(self):
        self.bans.check_if_banned(self.sender)

    """strip bot name from message"""
    def strip_bot_name(self):
        self.message = self.message.replace(self.config['botName'], '').strip()

    """split message"""
    def split_message(self):
        self.message = self.message.split(' ')
