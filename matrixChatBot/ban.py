import matrixChatBot.staff
from matrixChatBot.parse_users import ParseUsers


class Ban:
    def __init__(self):
        self.bans = ParseUsers.read_users('ban')

    def ban(self, sender, message):
        if matrixChatBot.staff.Mod.is_staff(sender) or sender in matrixChatBot.staff.Admin.is_staff(sender):
            if message[0] == 'ban' and message[1] not in self.bans:
                ParseUsers.write_to_file(message[1])
                return 'User ' + message[1] + ' is now banned'
            else:
                return 'User is already banned'

    def unban(self, sender, message):
        if matrixChatBot.staff.Mod.is_staff(sender) or sender in matrixChatBot.staff.Admin.is_staff(sender):
            if message[0] == 'unban' and message[1] in self.bans:
                self.remove_ban(message[1])
                return 'User ' + message[1] + ' is no longer banned'
            else:
                return 'User wasn\'t banned in the first place'

    def check_if_banned(self, user):
        return user in self.bans

    def remove_ban(self, message):
        pass
