"""Class to read an write users to a txt file. Used for bans and staff members
    users are saved in a dict
    {'ban': { 'user1': ['room1']},
    'mods': { 'user2': ['room1', 'room2'],
              'user3': ['room3'] }
    'admins': {'user4' : ['room1', 'room2', 'room3']}
"""
import json


class ParseUsers:
    def __init__(self):
        pass

    """Returns content of text file as a list"""
    @staticmethod
    def read_users(filter):
        with open('users.txt') as f:
            users = json.load(f)
        return users[filter]
        return user_dict

    """Write to text file"""
    @staticmethod

    def write_to_file(users):
        pass

    """Remove user from list"""
    @staticmethod
    def remove_from_file(user):
        pass
